<?php
/**
 * Created by PhpStorm.
 * User: marekmigas
 * Date: 28.06.16
 * Time: 11:12
 */

$config['displayErrorDetails'] = true;
$config['db']['host']   = "database";
$config['db']['user']   = "tm";
$config['db']['pass']   = "tm";
$config['db']['dbname'] = "tm";
// Monolog settings
$config['logger'] = [
    'name' => 'slim-app',
    'path' => __DIR__ . '/../logs/app.log',
    'level' => \Monolog\Logger::DEBUG,
];
