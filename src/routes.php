<?php
/**
 * Created by PhpStorm.
 * User: marekmigas
 * Date: 28.06.16
 * Time: 11:13
 */

use Slim\Http\Request;
use Slim\Http\Response;

function crypto_rand_secure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min;
    $log = ceil(log($range, 2));
    $bytes = (int)($log / 8) + 1;
    $bits = (int)$log + 1;
    $filter = (int)(1 << $bits) - 1;
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter;
    } while ($rnd > $range);
    return $min + $rnd;
}

function createToken($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet .= "abcdefghijklmnopqrstuvwxyz";
    $codeAlphabet .= "0123456789";
    $max = strlen($codeAlphabet);

    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[crypto_rand_secure(0, $max - 1)];
    }

    return $token;
}

//GET vytiahnutie vsetkych miestnosti z DB
$app->get('/auth/room/all', function (Request $request, Response $response) {
    $stmt = $this->db->query("SELECT * FROM room_picture ");
    $ret = [];
    while ($roomPicture = $stmt->fetch()) {
        $ret[] = $roomPicture;
    }
    return $response->withJson($ret);
});

//DELETE mazanie miestnosti so zvolenym ID
$app->delete('/auth/room/delete/{id_room_picture}', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (empty($args['id_room_picture'])) {
        return $response->withStatus(404)
            ->withJson(['message' => 'Non existing ID']);
    }
    $stmt = $this->db->prepare("DELETE FROM room_picture WHERE id_room_picture = :id_room_picture");
    $stmt->bindValue(':id_room_picture', $args['id_room_picture']);
    $stmt->execute();
    return $response->withStatus(200);
});

//POST vytvaranie novej miestnosti
$app->post('/auth/room', function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    if (empty($data['name']) || empty($data['num_picture']) || (intval($data['num_picture']) <= 0) ||
        empty($data['pictures']) || !is_array($data['pictures'])
    ) {
        return $response->withStatus(400)
            ->withJson(['message' => 'Name and number of pictures must be entered']);
    }
    //Query na vytvorenie miestnosti (room_picture)
    $this->db->beginTransaction();
    $stmt = $this->db->prepare("INSERT INTO room_picture (name, description, num_picture) VALUES (:name, :desc, :num_picture)");
    $stmt->bindValue(":name", $data["name"]);
    $stmt->bindValue(":desc", $data["description"]);
    $stmt->bindValue(":num_picture", $data["num_picture"]);
    $stmt->execute();
    $roomId = $this->db->lastInsertId();

    foreach ($data['pictures'] as $picture) {
        if (empty($picture['name']) || empty($picture['width']) ||
            empty($picture['height']) || empty($picture['image']) || ($picture['image'] == 'images/default.png')
        ) {
            $this->logger->info("Skipping empty image: " . var_export($picture, true));
            continue;
        }
        $stmt = $this->db->prepare(
            "INSERT INTO picture(name, width, height, image, room_picture_id_room_picture, pos) 
            VALUES (:name, :width, :height, :image, :room_id, :pos)"
        );
        $stmt->bindValue(':name', $picture['name']);
        $stmt->bindValue(':width', $picture['width']);
        $stmt->bindValue(':height', $picture['height']);
        $stmt->bindValue(':image', $picture['image']);
        $stmt->bindValue(':room_id', $roomId);
        $stmt->bindValue(':pos', $picture['pos']);
        $stmt->execute();
    }
    $this->db->commit();

    $stmt = $this->db->prepare("SELECT * FROM room_picture WHERE id_room_picture = :id");
    $stmt->bindValue(':id', $roomId);
    $stmt->execute();
    return $response->withJson([
        'success' => true,
        'room' => $stmt->fetch(),
        'info' => 'Pictures added successfully'
    ]);
});


// UPDATE miestnosti so zvolenym ID
$app->post('/auth/room/update/{id_room_picture}', function (Request $request, Response $response, $args) {
    if (empty($args['id_room_picture'])) {
        return $response->withStatus(404)
            ->withJson(['message' => 'Non existing ID']);
    }
    $data = $request->getParsedBody();
    if (empty($data['name']) || empty($data['num_picture']) || (intval($data['num_picture']) <= 0) ||
        empty($data['pictures']) || !is_array($data['pictures'])
    ) {
        return $response->withStatus(400)
            ->withJson(['message' => 'Name and number of pictures must be entered']);
    }
    $this->db->beginTransaction(); // UPDATE INFORMACII O MIESTNOSTI
    $stmt = $this->db->prepare("UPDATE room_picture SET name=:name, description=:desc WHERE id_room_picture=:id_room_picture");
    $stmt->bindValue('name', $data['name']);
    $stmt->bindValue('desc', $data['description']);
    $stmt->bindValue('id_room_picture', $args['id_room_picture']);
    $stmt->execute();

    foreach ($data['pictures'] as $picture) {
        if (empty($picture['name']) || empty($picture['width']) ||
            empty($picture['height']) || empty($picture['image']) || ($picture['image'] == 'images/default.png')
        ) {
            $this->logger->info("Skipping empty image: " . var_export($picture, true));
            continue;
        }
        if (($picture['image'] != 'images/default.png') && $picture['id'] == null) {
            $stmt = $this->db->prepare(
                "INSERT INTO picture(name, width, height, image, room_picture_id_room_picture, pos) 
            VALUES (:name, :width, :height, :image, :room_id, :pos)"
            );
            $stmt->bindValue(':name', $picture['name']);
            $stmt->bindValue(':width', $picture['width']);
            $stmt->bindValue(':height', $picture['height']);
            $stmt->bindValue(':image', $picture['image']);
            $stmt->bindValue(':room_id', $args['id_room_picture']);
            $stmt->bindValue(':pos', $picture['pos']);
            $stmt->execute();
            continue;

        }
        $stmt = $this->db->prepare("UPDATE picture SET name=:name, width=:width, height=:height, image=:image WHERE id_picture=:id_picture");
        $stmt->bindValue(':name', $picture['name']);
        $stmt->bindValue(':width', $picture['width']);
        $stmt->bindValue(':height', $picture['height']);
        $stmt->bindValue(':image', $picture['image']);
        $stmt->bindValue(':id_picture', $picture['id']);
        $stmt->execute();
    }
    $this->db->commit();
    return $response->withJson([
        'success' => true,
        'info' => 'Pictures updated successfully'
    ]);
});

// GET vytiahnutie miestnosti z DB so zadanym ID
$app->get('/auth/room/{id}', function (Request $request, Response $response, $args) {
    if (empty($args['id'])) {
        return $response->withStatus(404)
            ->withJson(['message' => 'Non existing ID']);
    }
    $stmt = $this->db->prepare(
        "SELECT id_room_picture, num_picture, name, description FROM room_picture WHERE id_room_picture=:id"
    );
    $stmt->bindValue(':id', $args['id']);
    $stmt->execute();
    $result = $stmt->fetch();
    $stmt = $this->db->prepare(
        "SELECT id_picture, name, description, width, height, image, pos FROM picture 
          WHERE picture.room_picture_id_room_picture=:id"
    );
    $stmt->bindValue(':id', $args['id']);
    $stmt->execute();
    $result['pictures'] = $stmt->fetchAll();
    return $response->withJson($result);
});

// GET predchadzajuca miestnost
$app->get('/room/previous/{id}', function (Request $request, Response $response, $args) {
    if (empty($args['id'])) {
        return $response->withStatus(404)
            ->withJson(['message' => 'Non existing ID']);
    }
    // data o miestnosti
    $stmt = $this->db->prepare("SELECT * FROM room_picture WHERE id_room_picture = (select max(id_room_picture) from room_picture where id_room_picture < :id)");
    $stmt->bindValue(':id', $args['id']);
    $stmt->execute();
    $result[0] = $stmt->fetch();
    // data o obrazoch
    $stmt1 = $this->db->prepare("SELECT id_room_picture, num_picture, room_picture.name AS room_name, room_picture.description AS room_description, picture.id_picture, picture.name AS picture_name, picture.description AS picture_description, picture.width, picture.height, picture.image, picture.pos FROM room_picture JOIN picture ON(room_picture.id_room_picture = picture.room_picture_id_room_picture) WHERE id_room_picture=:id");
    $stmt1->bindValue(':id', $result[0]['id_room_picture']);
    $stmt1->execute();
    $result[1] = $stmt1->fetchAll();

    return $response->withJson($result);
});

// GET nasledujuca miestnost
$app->get('/room/next/{id}', function (Request $request, Response $response, $args) {
    if (empty($args['id'])) {
        return $response->withStatus(404)
            ->withJson(['message' => 'Non existing ID']);
    }
    // data o miestnosti
    $stmt = $this->db->prepare("SELECT * FROM room_picture WHERE id_room_picture = (select min(id_room_picture) from room_picture where id_room_picture > :id)");
    $stmt->bindValue(':id', $args['id']);
    $stmt->execute();
    $result[0] = $stmt->fetch();
    // data o obrazoch
    $stmt1 = $this->db->prepare("SELECT id_room_picture, num_picture, room_picture.name AS room_name, room_picture.description AS room_description, picture.id_picture, picture.name AS picture_name, picture.description AS picture_description, picture.width, picture.height, picture.image, picture.pos FROM room_picture JOIN picture ON(room_picture.id_room_picture = picture.room_picture_id_room_picture) WHERE id_room_picture=:id");
    $stmt1->bindValue(':id', $result[0]['id_room_picture']);
    $stmt1->execute();
    $result[1] = $stmt1->fetchAll();

    return $response->withJson($result);
});


// init
$app->get('/init', function (Request $request, Response $response) {
    // prva miestnost
    $stmt3 = $this->db->prepare("SELECT id_room_picture, num_picture, room_picture.name AS room_name, room_picture.description AS room_description, picture.id_picture, picture.name AS picture_name, picture.description AS picture_description, picture.width, picture.height, picture.image, picture.pos FROM room_picture JOIN picture ON(room_picture.id_room_picture = picture.room_picture_id_room_picture) WHERE id_room_picture=(SELECT id_room_picture FROM room_picture ORDER BY id_room_picture ASC LIMIT 1) ORDER BY picture.pos");
    $stmt3->execute();
    $results[1] = $stmt3->fetchAll();
    return $response->withJson($results);
});

// detail o malbe so zadanym ID
$app->get('/room/details/{id}', function (Request $request, Response $response, $args) {
    if (empty($args['id'])) {
        return $response->withStatus(404)
            ->withJson(['message' => 'Non existing ID']);
    }
    $stmt = $this->db->prepare("SELECT * FROM picture WHERE id_picture=:id");
    $stmt->bindValue(':id', $args['id']);
    $stmt->execute();
    return $response->withJson($stmt->fetchAll());
});

$app->post('/login', function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    if (empty($data['login']) || empty($data['password'])) {
        return $response->withStatus(400)
            ->withJson(['message' => 'Both login and password must be entered']);
    }
    $stmt = $this->db->prepare("SELECT * FROM admin WHERE login_admin=:login AND pass_admin=:password");
    $stmt->bindValue(":login", $data['login']);
    $stmt->bindValue(":password", $data['password']);
    $stmt->execute();
    if ($stmt->fetch()) {
        $token = createToken(64);
        $stmt = $this->db->prepare("UPDATE admin SET token=:token, date_expired=date_add(now(), INTERVAL 1 hour) WHERE login_admin=:login");
        $stmt->bindValue('token', $token);
        $stmt->bindValue(':login', $data['login']);
        $stmt->execute();
        return $response->withJson([
            'message' => 'User logged in',
            'token' => $token
        ], 200);
    } else {
        return $response->withJson([
            'message' => 'Wrong login or password'
        ], 401);
    }
});

$app->post('/auth/logout/{user}', function (Request $request, Response $response, $args) {
    if (empty($args['user'])) {
        return $response->withStatus(404)
            ->withJson(['message' => 'Non existing user']);
    }
    $stmt = $this->db->prepare("UPDATE admin SET token='0' WHERE login_admin=:login");
    $stmt->bindValue(":login", $args['user']);
    $stmt->execute();
    return $response->withJson([
        'success' => true,
        'info' => 'User logged out successfully'
    ]);
});
