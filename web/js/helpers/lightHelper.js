/**
 * Created by marekmigas on 05.09.16.
 */

var lightHelper = {
    addLight: function (scene) {

        var target = new THREE.Mesh(new THREE.BoxGeometry(10, 10, 10), new THREE.MeshNormalMaterial());
        target.position.set(0, 11, -800);
        scene.add(target);

        var spotLight = new THREE.SpotLight(0xffffff);
        spotLight.castShadow = true;
        spotLight.shadowDarkness = 1;
        spotLight.distance = 1200;
        spotLight.shadow.camera.near = 1;
        spotLight.shadow.camera.far = 2000;
        spotLight.position.set(0, 350, -650);
        spotLight.angle = Math.PI / 9;
        spotLight.target = target;
        spotLight.penumbra = 0.3;
        spotLight.intensity = 1;
        scene.add(spotLight);
    },
    addLights: function (data, scene) {
        for (var i = 0; i < data.length; i++) {
            var posX = data[i].position.x, posZ = data[i].position.z;
            var lightX = 0, lightY = 350, lightZ = 0;
            //SpotLight( color, intensity, distance, angle, penumbra)
            var spotLight = new THREE.SpotLight(0xffffff, 2, 1200, Math.PI / 8, 0.3);
            //spotLight.castShadow = true;
            spotLight.shadowDarkness = 1;
            spotLight.shadow.camera.near = 1;
            spotLight.shadow.camera.far = 2000;
            spotLight.target = data[i];
            if (posX === 0 && posZ < 0) {
                posZ += 180;
            } else if ((posX > 0 && posZ < 0) || (posX > 0 && posZ > 0) || (posX > 0 && posZ === 0)) {
                posX -= 150;
            } else if (posX === 0 && posZ > 0) {
                posZ -= 150;
            } else if ((posX < 0 && posZ > 0) || (posX < 0 && posZ < 0) || (posX < 0 && posZ === 0)) {
                posX += 150;
            }
            //spotLight.name = data[i].name + 'light';
            spotLight.position.set(posX, lightY, posZ);
            //scene.add(spotLight);

            pointLight = new THREE.PointLight(0xffffff, 1.2, 1000);
            pointLight.name = data[i].name + 'light';
            scene.add(pointLight);
            pointLight.position.set(posX, lightY, posZ);
            sphere = new THREE.SphereGeometry(5, 16, 8, 1);

            lightMesh = new THREE.Mesh(sphere, new THREE.MeshBasicMaterial({color: 0xffffff}));
            lightMesh.name = data[i].name + 'bulb';
            scene.add(lightMesh);
            lightMesh.position.set(posX, lightY, posZ);

        }
    },
    removeLight: function (name, scene) {
        scene.remove(scene.getObjectByName(name));
    },
    removeBulbs: function (name, scene) {
        scene.remove(scene.getObjectByName(name));
    },
    lightOff: function (name, scene) {
        var light = scene.getObjectByName(name);
        if (light.intensity >= 1.4) {
            for (var i = 0; i < 100; i++) {
                (function (i) {
                    setTimeout(function () {
                        light.intensity -= 0.01;
                    }, 15 * i)
                })(i);
            }
        }
    },
    lightUp: function (name, scene) {
        var light = scene.getObjectByName(name);
        if (light.intensity <= 0.4) {
            for (var i = 0; i < 100; i++) {
                (function (i) {
                    setTimeout(function () {
                        light.intensity += 0.01;
                    }, 15 * i)
                })(i);
            }
        }
    },
    addBulbs: function () {
        var bulbs = [];

        var bulbGeometry = new THREE.SphereGeometry(15, 16, 8);
        var SEbulb = new THREE.PointLight(0xffee88, 1.2, 1000, 2);
        var SWbulb = new THREE.PointLight(0xffee88, 1.2, 1000, 2);
        var NEbulb = new THREE.PointLight(0xffee88, 1.2, 1000, 2);
        var NWbulb = new THREE.PointLight(0xffee88, 1.2, 1000, 2);
        var material = new THREE.MeshStandardMaterial({
            emissive: 0xffffee,
            emissiveIntensity: 1,
            color: 0x000000
        });

        SEbulb.add(new THREE.Mesh(bulbGeometry, material));
        SWbulb.add(new THREE.Mesh(bulbGeometry, material));
        NEbulb.add(new THREE.Mesh(bulbGeometry, material));
        NWbulb.add(new THREE.Mesh(bulbGeometry, material));

        SEbulb.position.set(147, 270, 294);
        SWbulb.position.set(147, 270, -306);
        NEbulb.position.set(-153, 270, 294);
        NWbulb.position.set(-153, 270, -306);

        bulbs.push(SEbulb);
        bulbs.push(SWbulb);
        bulbs.push(NEbulb);
        bulbs.push(NWbulb);

        return bulbs;
    },
    addLampDetails: function () {
        var cables = [];

        var cableGeometry = new THREE.CylinderGeometry(1, 1, 300, 32);
        var cableMaterial = new THREE.MeshPhongMaterial({color: 0x737373}, {shininess: 100});

        for (var i = 0; i <= 3; i++) {
            var cable = new THREE.Mesh(cableGeometry, cableMaterial);
            cables.push(cable);
        }

        cables[0].position.set(147, 420, 294);
        cables[1].position.set(147, 420, -306);
        cables[2].position.set(-153, 420, 294);
        cables[3].position.set(-153, 420, -306);

        return cables;
    }
};
