/**
 * Created by marekmigas on 05.09.16.
 */

// pomocna trieda na vykreslovanie a mazanie obrazov

var paintingHelper = {
    // funkcia na vykreslenie 1 obrazu
    // vracia jeden THREE.Mesh() objekt
    drawPainting: function (data) {
        var paintingGeometry = new THREE.BoxGeometry(1, 1, 1); // geometria obrazu
        var paintingMaterial = new THREE.MeshPhongMaterial({
            map: THREE.ImageUtils.loadTexture(data['image']) // pridane textury k materialu obrazu
        });
        var painting = new THREE.Mesh(paintingGeometry, paintingMaterial);
        painting.scale.set(data['width'] * 2, data['height'] * 2, 1);
        painting.name = data['id_picture'];
        painting.castShadow = true;
        painting.receiveShadow = true;

        return painting;
    },

    //funkcia na vymazanie 1 obrazu zo sceny
    //vstupne parametre su meno, a scena
    removePainting: function (name, scene) {
        scene.remove(scene.getObjectByName(name));
    },

    // funkcia na vykreslenie lubovolneho poctu obrazov
    // vstupne paramerte su data z DB a scena
    // funkcia zaroven vykresli aj ram pre obraz
    // podporovane je rozmiestnenie pre 4 alebo 6 obrazov, pre viac sa da dorobit
    // funkcia vracia pole obrazov
    drawPaintings: function (data, scene) {
        var paintingMx = 0, paintingMz = -800, paintingMy = 20; // Pozicia obrazov
        var paintings = [];
        var paintingCount = data[0]['num_picture'];
        var help_array = [];
        for (var i = 0; i < paintingCount; i++) {
            help_array.push({
                'index': i,
                'id': null,
                'name': 'empty',
                'width': '1',
                'height': '1',
                'image': "images/default.png",
                'description': '',
                'pos': i
            });
        }
        for (var i = 0; i < paintingCount; i++) {
            if (data[i]) {
                help_array[data[i]['pos']] = data[i];
            }
        }
        for (var i = 0; i < paintingCount; i++) {
            if (help_array[i]) {
                if (help_array[i].name == 'empty') {
                    var paintingGeometry = new THREE.BoxGeometry(1, 1, 1); // geometria obrazu
                    var paintingMaterial = new THREE.MeshPhongMaterial({
                    color: 0xffffff // pridane textury k materialu obrazu
                    });
                    var painting = new THREE.Mesh(paintingGeometry, paintingMaterial);
                } else {
                    var texture = new THREE.TextureLoader().load(help_array[i]['image']);
                    var paintingGeometry = new THREE.BoxGeometry(1, 1, 1); // geometria obrazu
                    var paintingMaterial = new THREE.MeshPhongMaterial({
                        map: texture // pridane textury k materialu obrazu
                    });
                    var painting = new THREE.Mesh(paintingGeometry, paintingMaterial);
                    painting.scale.set(help_array[i]['width'] * 2, help_array[i]['height'] * 2, 1);
                    painting.name = help_array[i]['id_picture'];
                    painting.castShadow = true;
                    painting.receiveShadow = true;
                }
                painting.position.set(paintingMx, paintingMy, paintingMz);
                if (paintingCount == 4) {
                    switch (i) {
                        case 0:
                            paintingMx += 500;
                            paintingMz += 800;
                            break;
                        case 1:
                            paintingMx -= 500;
                            paintingMz += 800;
                            painting.rotation.y = 0.5 * Math.PI;
                            break;
                        case 2:
                            paintingMx -= 500;
                            paintingMz -= 800;
                            break;
                        case 3:
                            painting.rotation.y = 0.5 * Math.PI;
                            break;
                        default:
                            break;
                    }
                    //frameHelper.drawFrame(painting, scene);
                    paintings.push(painting);
                } else if (paintingCount == 6) {
                    switch (i) {
                        case 0:
                            paintingMx += 500;
                            paintingMz += 450;
                            break;
                        case 1:
                            paintingMz += 700;
                            painting.rotation.y = 0.5 * Math.PI;
                            break;
                        case 2:
                            paintingMx -= 500;
                            paintingMz += 450;
                            painting.rotation.y = 0.5 * Math.PI;
                            break;
                        case 3:
                            paintingMx -= 500;
                            paintingMz -= 450;
                            break;
                        case 4:
                            paintingMz -= 700;
                            painting.rotation.y = 0.5 * Math.PI;
                            break;
                        case 5:
                            painting.rotation.y = 0.5 * Math.PI;
                            break;
                        default:
                            scene.remove(painting);
                    }
                    //frameHelper.drawFrame(painting, scene);
                    paintings.push(painting);
                }
            } else {
                continue;
            }
        }
        return paintings;
    }
};