/**
 * Created by marekmigas on 19.3.17.
 */

/**
 * trieda sluziaca na vytvaranie, rozmiestnenie a pridanie textur
 * stenam miestnosti.
 *
 * @return walls; Metoda buildWalls vracia pole stien /walls/
 *
 * @type {{buildWalls: wallsBuilder.buildWalls}}
 */

var wallsBuilder = {
    buildWalls: function () {
        var walls = [];
        var sideWallsGeometry = new THREE.BoxGeometry(1, 600, 1610);
        var frontBackWallGeometry = new THREE.BoxGeometry(1010, 600, 1);

        var loader = new THREE.TextureLoader();

        var leftWall, rightWall, frontWall, backWall;

        loader.load(
            '../../images/concreteWall.jpg',
            function (texture) {
                var wallMaterial = new THREE.MeshBasicMaterial({
                    map: texture
                });
                leftWall = new THREE.Mesh(sideWallsGeometry, wallMaterial);
                rightWall = new THREE.Mesh(sideWallsGeometry, wallMaterial);
                frontWall = new THREE.Mesh(frontBackWallGeometry, wallMaterial);
                backWall = new THREE.Mesh(frontBackWallGeometry, wallMaterial);

                leftWall.position.set(-505, 100, 0);
                rightWall.position.set(505, 100, 0);
                frontWall.position.set(0, 100, -805);
                backWall.position.set(0, 100, 805);

                scene.add(leftWall);
                scene.add(rightWall);
                scene.add(leftWall);
                scene.add(leftWall);
            }
        );

        return walls;

    }
};