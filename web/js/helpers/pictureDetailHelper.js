/**
 * Created by marekmigas on 07.09.16.
 */

var pictureDetailHelper = {
    showDetails: function (data) {
        $(document).ready(function () {
            //document.getElementById('paintingDetails').style.display = 'inline';
            $('#paintingDetails').css('display', 'inline');
            $('#paintingDetailsContent').fadeIn(1500);
            //document.getElementById('paintingName').innerHTML = data['name'];
            $('#paintingName').html(data['name']);
            //document.getElementById('paintingImg').src = data['image'];
            $('#paintingImg').attr('src', data['image']);
            $('#paintingMeasurements').html(data['width'] + ' x ' + data['height']);
        });
    },
    hideDetails: function () {
        $(document).ready(function () {
            $('#paintingDetailsContent').fadeOut(1000);
            $('#paintingDetails').fadeOut(2000);
        });
    }
};