/**
 * Created by marekmigas on 04.09.16.
 */

var frameHelper = {
    drawFrame: function (painting, scene) {
        var width = painting.scale.x;
        var height = painting.scale.y;
        var posX = painting.position.x, posY = painting.position.y, posZ = painting.position.z;
        var verticalLX = 0, verticalLY = 0, verticalLZ = 0;
        var verticalRX = 0, verticalRY = 0, verticalRZ = 0;
        var horizontalLX = 0, horizontalLY = (posY - (height / 2)) - 2.5, horizontalLZ = 0;
        var horizontalUX = 0, horizontalUY = (posY + (height / 2)) + 2.5, horizontalUZ = 0;
        var rotY = painting.rotation.y;
        var material = new THREE.MeshPhongMaterial({color: 0xff0000});
        var geometry = new THREE.BoxGeometry(1, 1, 1);

        if (posX === 0 && posZ < 0) {
            posZ -= 1;
            verticalLZ = posZ + 2;
            verticalRZ = posZ + 2;
            verticalLX = (posX - (width / 2)) - 2;
            verticalRX = (posX + (width / 2)) + 2;
            horizontalLZ = posZ + 2;
            horizontalUZ = posZ + 2;
        } else if ((posX > 0 && posZ < 0) || (posX > 0 && posZ > 0) || (posX > 0 && posZ === 0)) {
            posX += 1;
            verticalLX = posX - 2;
            verticalRX = posX - 2;
            verticalLZ = (posZ - (width / 2)) - 2;
            verticalRZ = (posZ + (width / 2)) + 2;
            horizontalLX = posX - 2;
            horizontalUX = posX - 2;
            horizontalLZ = posZ;
            horizontalUZ = posZ;
        } else if (posX === 0 && posZ > 0) {
            posZ += 1;
            verticalLZ = posZ - 2;
            verticalRZ = posZ - 2;
            verticalLX = (posX - (width / 2)) - 2;
            verticalRX = (posX + (width / 2)) + 2;
            horizontalLZ = posZ - 2;
            horizontalUZ = posZ - 2;
        } else if ((posX < 0 && posZ > 0) || (posX < 0 && posZ < 0) || (posX < 0 && posZ === 0)) {
            posX -= 1;
            verticalLZ = (posZ - (width / 2)) - 2;
            verticalRZ = (posZ + (width / 2)) + 2;
            verticalLX = posX + 2;
            verticalRX = posX + 2;
            horizontalLZ = posZ;
            horizontalUZ = posZ;
            horizontalLX = posX + 2;
            horizontalUX = posX + 2;
        }

        var back = new THREE.Mesh(geometry, material);
        back.scale.set(width, height, 1);
        back.position.set(posX, posY, posZ);
        back.rotation.y = rotY;
        var verticalL = new THREE.Mesh(geometry, material);
        var verticalR = new THREE.Mesh(geometry, material);
        var horizontalL = new THREE.Mesh(geometry, material);
        var horizontalU = new THREE.Mesh(geometry, material);
        verticalL.scale.set(5, height + 10, 4);
        verticalR.scale.set(5, height + 10, 4);
        horizontalL.scale.set(width, 5, 4);
        horizontalU.scale.set(width, 5, 4);
        verticalL.position.set(verticalLX, posY, verticalLZ);
        verticalR.position.set(verticalRX, posY, verticalRZ);
        horizontalL.position.set(horizontalLX, horizontalLY, horizontalLZ);
        horizontalU.position.set(horizontalUX, horizontalUY, horizontalUZ);
        verticalL.rotation.y = rotY;
        verticalR.rotation.y = rotY;
        horizontalL.rotation.y = rotY;
        horizontalU.rotation.y = rotY;
        var group = new THREE.Object3D();
        group.name = painting.name + 'frame';
        group.add(back, verticalL, verticalR, horizontalL, horizontalU);
        scene.add(group);
    },
    removeObject: function (name, scene) {
        scene.remove(scene.getObjectByName(name));
    }
};
