/**
 * Created by marekmigas on 10.09.16.
 */

var animations = {
    showPictureDetails: function (data) {
        $(document).ready(function () {
            $('#paintingName').html(data['name']);
            $('#paintingImg').attr('src', data['image']);
            $('#paintingMeasurements').html(data['width'] + ' x ' + data['height']);
            $('#paintingDetails').animate({
                left: '0%',
                zIndex: 1001
            }, 700);
            $('#paintingDetailsContent').animate({
                left: '0%',
                zIndex: 1001
            }, 700);
        });
    },
    hidePictureDetails: function () {
        $(document).ready(function () {
            $('#paintingDetails').animate({
                left: '100%'
            }, 700);
            $('#paintingDetailsContent').animate({
                left: '100%'
            }, 700, function () {
                $('#paintingDetails').css('left', '-100%');
                $('#paintingDetailsContent').css('left', '-100%');
            });
        });
    },
    showHelp: function () {
        $(document).ready(function () {
           $('#help-popup').toggleClass('controls-hidden');
        });
    }
};
