/**
 * Created by marekmigas on 11.09.16.
 */
$(document).ready(function () {
    $('#wrap').mouseenter(function () {
        if (!$('.menu').hasClass('menuOpened')) {
            $('.d1').addClass('d1Hover');
            $('.d3').addClass('d3Hover');
        } else {
            $('.d1').addClass('d1OpenedHover');
            $('.d2').addClass('d2OpenedHover');
            $('.d3').addClass('d3OpenedHover');
        }
    });
    $('#wrap').mouseleave(function () {
        if (!$('.menu').hasClass('menuOpened')) {
            $('.d1').removeClass('d1Hover');
            $('.d3').removeClass('d3Hover');
        } else {
            $('.d1').removeClass('d1OpenedHover');
            $('.d2').removeClass('d2OpenedHover');
            $('.d3').removeClass('d3OpenedHover');
        }
    });
    $('#wrap').click(function () {
        $('.menu').toggleClass('menuOpened');
        $('.menuBack').toggleClass('menuOpened');
        $('.navBar').toggleClass('navBarOpened');
        if ($('.menu').hasClass('menuOpened')) {
            $('.d1').removeClass('d1Hover');
            $('.d3').removeClass('d3Hover');
            $('.d1').toggleClass('d1Opened');
            $('.d2').toggleClass('d2Opened');
            $('.d3').toggleClass('d3Opened');
        } else {
            $('.d1').removeClass('d1OpenedHover');
            $('.d2').removeClass('d1OpenedHover');
            $('.d3').removeClass('d3OpenedHover');
            $('.d1').toggleClass('d1Opened');
            $('.d2').toggleClass('d2Opened');
            $('.d3').toggleClass('d3Opened');
        }
    });

    $("#about").click(function () {
        $('.aboutSection').toggleClass('aboutSection-visible');
    });
    $(".closeIcon").click(function () {
        $('.aboutSection').toggleClass('aboutSection-visible');
    });

    $(".back").click(function () {
        window.location.href = "virtualGallery.html";
    });

});