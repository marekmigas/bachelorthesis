/**
 * Created by marekmigas on 29.07.16.
 */
'use strict';

angular.module('index')
    .directive('ngWebgl', function ($http, $rootScope) {
        return {
            restrict: 'A',
            scope: {
                'vysl': '='
            },
            link: function postLink(scope, element, attrs) {

                $rootScope.paintings = [];
                $rootScope.detailsOpened = false;
                $rootScope.pointerLock = false;

                var camera, scene, renderer;
                var controls;
                var stats;

                var blocker = document.getElementById('blocker');
                var instructions = document.getElementById('instructions');
                var havePointerLock = 'pointerLockElement' in document || 'mozPointerLockElement' in document || 'webkitPointerLockElement' in document;

                // Ak prehliadac ma PointerLock API
                if (havePointerLock) {
                    var element = document.body;
                    var pointerlockchange = function (event) {

                        if (document.pointerLockElement === element || document.mozPointerLockElement === element || document.webkitPointerLockElement === element) {
                            controlsEnabled = true;
                            controls.enabled = true;
                            blocker.style.display = 'none';
                        } else {
                            controls.enabled = false;
                            blocker.style.display = '-webkit-box';
                            blocker.style.display = '-moz-box';
                            blocker.style.display = 'box';
                            instructions.style.display = '';
                            $rootScope.pointerLock = false;
                            if ($rootScope.detailsOpened) {
                                animations.hidePictureDetails();
                                $rootScope.detailsOpened = false;
                            }
                        }
                    };

                    var pointerlockerror = function (event) {
                        instructions.style.display = '';
                    };

                    // Pridanie stavu PointerLock - u k eventom
                    document.addEventListener('pointerlockchange', pointerlockchange, false);
                    document.addEventListener('mozpointerlockchange', pointerlockchange, false);
                    document.addEventListener('webkitpointerlockchange', pointerlockchange, false);
                    document.addEventListener('pointerlockerror', pointerlockerror, false);
                    document.addEventListener('mozpointerlockerror', pointerlockerror, false);
                    document.addEventListener('webkitpointerlockerror', pointerlockerror, false);
                    instructions.addEventListener('click', function (event) {

                        instructions.style.display = 'none';
                        $rootScope.pointerLock = true;


                        // Zadanie prehliadaca o PointerLock
                        element.requestPointerLock = element.requestPointerLock || element.mozRequestPointerLock || element.webkitRequestPointerLock;

                        if (/Firefox/i.test(navigator.userAgent)) {

                            var fullscreenchange = function (event) {

                                if (document.fullscreenElement === element || document.mozFullscreenElement === element || document.mozFullScreenElement === element) {

                                    document.removeEventListener('fullscreenchange', fullscreenchange);
                                    document.removeEventListener('mozfullscreenchange', fullscreenchange);

                                    element.requestPointerLock();
                                }
                            };

                            document.addEventListener('fullscreenchange', fullscreenchange, false);
                            document.addEventListener('mozfullscreenchange', fullscreenchange, false);

                            element.requestFullscreen = element.requestFullscreen || element.mozRequestFullscreen || element.mozRequestFullScreen || element.webkitRequestFullscreen;

                            element.requestFullscreen();
                        } else {
                            element.requestPointerLock();
                        }

                    }, false);

                } else {
                    // Oznamenie uzivatelovi o tom, ze jeho prehliadac nedisponuje PointerLock API
                    instructions.innerHTML = 'Your browser doesn\'t seem to support Pointer Lock API';
                }

                // ========================= GLOBALNE PREMENNE ========================= //
                var controlsEnabled = false;

                var paintingMx = 0, paintingMz = -800, paintingMy = 11; // Pozicia obrazov

                // pohyb
                var moveForward = false;
                var moveBackward = false;
                var moveLeft = false;
                var moveRight = false;
                var prevTime = performance.now();
                var velocity = new THREE.Vector3();
                var arrow = 0; // Arrow Helper

                var lastAvailableX = 0;
                var lastAvailableZ = 0;


                var firstClick = -1;

                // premenne na zistovamie intersekcie pre zobrazovnie detailov o obraze na ktory sa klikne
                var isRaycaster;
                var mouse;
                var paintings;

                // Kamera
                camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 2000);

                // Scena
                scene = new THREE.Scene();

                // PointerLock API
                controls = new THREE.PointerLockControls(camera);
                scene.add(controls.getObject());
                // POHYB
                var onKeyDown = function (event) {
                    switch (event.keyCode) {
                        case 38: // up
                        case 87: // w
                            moveForward = true;
                            break;
                        case 37: // left
                        case 65:// a
                            moveLeft = true;
                            break;
                        case 40: // down
                        case 83: // s
                            moveBackward = true;
                            break;
                        case 39: // right
                        case 68: // d
                            moveRight = true;
                            break;
                    }
                };
                var onKeyUp = function (event) {
                    switch (event.keyCode) {
                        case 38: // up
                        case 87: // w
                            moveForward = false;
                            break;
                        case 37: // left
                        case 65: // a
                            moveLeft = false;
                            break;
                        case 40: // down
                        case 83: // s
                            moveBackward = false;
                            break;
                        case 39:// right
                        case 68: // d
                            moveRight = false;
                            break;
                    }
                };

                document.addEventListener('keydown', onKeyDown, false);
                document.addEventListener('keyup', onKeyUp, false);

                var light = new THREE.AmbientLight(0x404040, 1.3); // soft white light
                scene.add(light);

                var cables = lightHelper.addLampDetails();
                var bulbs = lightHelper.addBulbs(); // ziarovky
                // instancia Loadera
                var loader = new THREE.JSONLoader();
                loader.load( // nacitavanie JSON objektu
                    '../images/bulb.js', // URL objektu
                    function (geometry, materials) { // ak sa objekt nacita...
                        loader.load(
                            '../images/lamp_texture.jpg',
                            function (texture) {
                                var lampMaterial = new THREE.MeshBasicMaterial({
                                    map: texture
                                });
                                var lamps = [];
                                for (var i = 0; i <= 3; i++) {
                                    var lamp = new THREE.Mesh(geometry, lampMaterial);
                                    lamp.rotation.x = Math.PI;
                                    lamp.scale.set(4, 4, 4);
                                    lamps.push(lamp);
                                    scene.add(lamp);
                                    scene.add(bulbs[i]);
                                    scene.add(cables[i]);
                                }
                                lamps[0].position.set(150, 300, 300);
                                lamps[1].position.set(150, 300, -300);
                                lamps[2].position.set(-150, 300, 300);
                                lamps[3].position.set(-150, 300, -300);
                            }
                        );
                        //var material = new THREE.MeshPhongMaterial({color: 0x666666}, {shininess: 100});

                    }
                );

                //walls
                var sideWallsGeometry = new THREE.BoxGeometry(1, 800, 1610);
                var frontBackWallGeometry = new THREE.BoxGeometry(1010, 800, 1);

                var loader = new THREE.TextureLoader();

                var leftWall, rightWall, frontWall, backWall;

                loader.load(
                    '../images/concreteWall_texture.jpg',
                    function (texture) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.offset.set(0, 0);
                        texture.repeat.set(10, 6);
                        var wallMaterial = new THREE.MeshBasicMaterial({
                            map: texture
                        });
                        leftWall = new THREE.Mesh(sideWallsGeometry, wallMaterial);
                        rightWall = new THREE.Mesh(sideWallsGeometry, wallMaterial);
                        frontWall = new THREE.Mesh(frontBackWallGeometry, wallMaterial);
                        backWall = new THREE.Mesh(frontBackWallGeometry, wallMaterial);

                        leftWall.position.set(-505, 100, 0);
                        rightWall.position.set(505, 100, 0);
                        frontWall.position.set(0, 100, -805);
                        backWall.position.set(0, 100, 805);

                        scene.add(leftWall);
                        scene.add(rightWall);
                        scene.add(frontWall);
                        scene.add(backWall);
                    }
                );


                //floor
                var floorGeometry = new THREE.BoxGeometry(1510, 1, 2010);
                loader.load(
                    '../images/concreteFloor_texture.jpg',
                    function (texture) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.offset.set(0, 0);
                        texture.repeat.set(1, 1);
                        var floorMaterial = new THREE.MeshBasicMaterial({
                            map: texture
                        });
                        var floor = new THREE.Mesh(floorGeometry, floorMaterial);
                        floor.position.set(0, -200, 0);
                        scene.add(floor);
                    }
                );
                //ceiling
                loader.load(
                    '../images/ceiling_texture.jpg',
                    function (texture) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.offset.set(0, 0);
                        texture.repeat.set(2, 1);
                        var floorMaterial = new THREE.MeshBasicMaterial({
                            map: texture
                        });
                        var floor = new THREE.Mesh(floorGeometry, floorMaterial);
                        floor.position.set(0, 550, 0);
                        scene.add(floor);
                    }
                );

                //accessories
                var woodGeo = new THREE.BoxGeometry(1150, 30, 30);
                var woodGeo2 = new THREE.BoxGeometry(30, 30, 2000);
                loader.load(
                    '../images/wood_texture.jpg',
                    function (texture) {
                        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
                        texture.offset.set(0, 0);
                        texture.repeat.set(10, 1);
                        var woodMaterial = new THREE.MeshBasicMaterial({
                            map: texture
                        });
                        var wood = new THREE.Mesh(woodGeo, woodMaterial);
                        wood.position.set(0, 550, 300);
                        scene.add(wood);
                        var wood2 = new THREE.Mesh(woodGeo, woodMaterial);
                        wood2.position.set(0, 550, -300);
                        scene.add(wood2);
                        var wood3 = new THREE.Mesh(woodGeo2, woodMaterial);
                        wood3.position.set(-350, 500, 0);
                        scene.add(wood3);
                        var wood4 = new THREE.Mesh(woodGeo2, woodMaterial);
                        wood4.position.set(350, 500, 0);
                        scene.add(wood4);

                    }
                );
                var errorHandler = function (data, status, header, config) {
                    if (data.message) {
                        alert("Failed to load application: " + status + ' ' + data.message);
                    } else {
                        alert("Failed to load application: " + status);
                    }
                    console.log(data);
                    console.log(status);
                    console.log(header);
                    console.log(config);
                };

                // KONIEC OSVETLENIA
                // ========================================================== /init ==========================================================
                // Inicializacia... nacitanie prvej miestnosti
                $http.get('/init')
                    .success(function (data) {
                            scope.init = function () {
                                //controls.getObject().translateX(velocity.x * delta)
                                controls.getObject().position.set(0, 10, 320);
                                paintings = [];
                                var paintingData = data[1];
                                var paintingArray = paintingHelper.drawPaintings(paintingData, scene);
                                for (var i = 0; i < paintingArray.length; i++) {
                                    scene.add(paintingArray[i]);
                                    paintings.push(paintingArray[i]);
                                    $rootScope.paintings.push(paintingArray[i]);
                                }

                                stats = new Stats();
                                stats.showPanel(1); // 0: fps, 1: ms, 2: mb, 3+: custom
                                //document.body.appendChild( stats.dom );

                                //lightHelper.addLights($rootScope.paintings, scene);
                                // Intersekcia objektov na zobrazovanie detailov
                                isRaycaster = new THREE.Raycaster();
                                isRaycaster.near = 1;
                                mouse = new THREE.Vector3();


                                // Vytvorenie a konfiguracia renderu
                                renderer = new THREE.WebGLRenderer();
                                renderer.setClearColor(0x000000);

                                renderer.gammaInput = true;
                                renderer.gammaOutput = true;

                                renderer.setPixelRatio(window.devicePixelRatio);
                                renderer.setSize(window.innerWidth, window.innerHeight);
                                document.body.appendChild(renderer.domElement);

                                //==================    INTERSEKCIE     =================
                                document.addEventListener('mousedown', onDocumentMouseDown, false);
                                document.addEventListener('touchstart', onDocumentTouchStart, false);

                                function onDocumentTouchStart(event) {
                                    event.preventDefault();
                                    event.clientX = event.touches[0].clientX;
                                    event.clientY = event.touches[0].clientY;
                                }

                                function onDocumentMouseDown(event) {
                                    event.preventDefault();
                                    mouse.x = 0;
                                    mouse.y = 0;
                                    mouse.z = 0;

                                    isRaycaster.setFromCamera(mouse, camera);
                                    scene.remove(arrow);
                                    arrow = new THREE.ArrowHelper(camera.getWorldDirection(), camera.getWorldPosition(), 400, Math.random() * 0xffffff);
                                    //scene.add(arrow);
                                    var paintingsIntersects = isRaycaster.intersectObjects($rootScope.paintings);

                                    if (paintingsIntersects.length > 0) {
                                        scope.displayPicture(paintingsIntersects[0].object.name);
                                    }
                                }//===================================
                            };

                            scope.render = function () {
                                renderer.render(scene, camera);
                                scope.handleResize();
                            };


                            scope.handleResize = function () {
                                camera.aspect = window.innerWidth / window.innerHeight;
                                camera.updateProjectionMatrix();
                                renderer.setSize(window.innerWidth, window.innerHeight);
                            };

                            scope.animate = function () {

                                stats.begin();
                                stats.end();

                                requestAnimationFrame(scope.animate);

                                if (controlsEnabled) {
                                    isRaycaster.ray.origin.copy(controls.getObject().position);
                                    if ((controls.getObject().position.x <= 495)
                                        && (controls.getObject().position.x >= -495)
                                        && (controls.getObject().position.z <= 805)
                                        && (controls.getObject().position.z >= -805)) {
                                        lastAvailableX = controls.getObject().position.x;
                                        lastAvailableZ = controls.getObject().position.z;

                                    } else {
                                        controls.getObject().position.set(lastAvailableX, 10, lastAvailableZ);
                                    }


                                    // POHYB
                                    var time = performance.now();
                                    var delta = ( time - prevTime ) / 1000;

                                    velocity.x -= velocity.x * 5.0 * delta;
                                    velocity.z -= velocity.z * 5.0 * delta;

                                    if (moveForward) {
                                        velocity.z -= 800.0 * delta;
                                    }
                                    if (moveBackward) {
                                        velocity.z += 800.0 * delta;
                                    }
                                    if (moveLeft) {
                                        velocity.x -= 800.0 * delta;
                                    }
                                    if (moveRight) {
                                        velocity.x += 800.0 * delta;
                                    }
                                    controls.getObject().translateX(velocity.x * delta);
                                    controls.getObject().translateY(velocity.y * delta);
                                    controls.getObject().translateZ(velocity.z * delta);
                                    prevTime = time;
                                }
                                renderer.render(scene, camera);
                            };


                            scope.init();
                            scope.animate();

                            $rootScope.currentRoomID = data[1][0]['id_room_picture'];  // id miestnosti v ktorej sa prave nachadza uzivatel
                            $rootScope.currentRoomNumPic = data[1][0]['num_picture'];  // pocet obrazov v konkretnej miestnosti
                        }
                    ).error(errorHandler);
                //========================================================== Koniec /init =========================================================

                // Zobrazenie detailov o obraze na ktory klikne uzivatel
                scope.displayPicture = function (id) {
                    if (firstClick >= 0 && $rootScope.pointerLock) {
                        var idStr = id.toString();
                        $http.get('/room/details/' + idStr)
                            .success(function (data) {
                                $rootScope.pictureDetails = data[0];
                                animations.showPictureDetails(data[0]);
                                $rootScope.detailsOpened = true;
                            })
                            .error(errorHandler);
                    } else {
                        firstClick += 1;
                    }

                };
                // Koniec zobrazovania detailov o obraze na ktory klikne uzivatel

                // ================================== Prepinanie miestnosti ==================================


                var changeRoom = function (event) {
                    var keyPressed;
                    keyPressed = String.fromCharCode(event.keyCode).toLowerCase();
                    switch (keyPressed) { // zistovanie kodu stlacenej klavesi a nasledne spracovanie(dalsia alebo predchodzia miestnost)
                        case 'l':
                            $http.get('/room/next/' + $rootScope.currentRoomID)
                                .success(function (data) {
                                    scope.redrawRoom(data);
                                    $rootScope.pictureDetails = [];
                                })
                                .error(errorHandler);
                            break;
                        case 'k':
                            $http.get('/room/previous/' + $rootScope.currentRoomID)
                                .success(function (data) {
                                    scope.redrawRoom(data);
                                })
                                .error(errorHandler);
                            break;
                        case 'c':
                            if ($rootScope.detailsOpened) {
                                animations.hidePictureDetails();
                                $rootScope.detailsOpened = false;
                            }
                            break;
                        case 'h':
                            animations.showHelp();
                            break;
                        default:
                            break;
                    }
                };
                document.addEventListener('keypress', changeRoom);
                // =============================== Koniec prepinania miestnosti ===============================

                // =============================== Prekreslovanie miestnosti =====================================
                scope.redrawRoom = function (data) {
                    if (data[0]['id_room_picture'] != null) { // ak existuje dalsia alebo predchadzajuca miestnost
                        for (var i = 0; i < $rootScope.paintings.length; i++) { // zmazanie ramov
                            frameHelper.removeObject($rootScope.paintings[i].name + 'frame', scene);
                            paintingHelper.removePainting($rootScope.paintings[i].name, scene);
                        }
                        var paintingArray = paintingHelper.drawPaintings(data[1], scene);
                        $rootScope.paintings = [];
                        for (var i = 0; i < paintingArray.length; i++) {
                            scene.add(paintingArray[i]);
                            $rootScope.paintings.push(paintingArray[i]);
                        }
                        $rootScope.currentRoomID = data[0]['id_room_picture']; // nastavenie ID aktualnej miestnosti
                        $rootScope.currentRoomNumPic = data[0]['num_picture']; // nastavenie poctu obrazov aktualnej miestnosti

                    } else {
                        Materialize.toast('No previous or next room...', 4000);
                    }
                };  // =============================== Koniec prekreslovania miestnosti ==============================
            }
        };
    });