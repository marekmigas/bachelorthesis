/**
 * Created by marekmigas on 28.06.16.
 */
var GalleryApp = angular.module('GalleryApp', ['GalleryAppControllers', 'ngRoute']);

GalleryApp.constant('apiURL', 'auth/room');

GalleryApp.directive('customOnChange', function () { /* https://stackoverflow.com/a/19647381/41640 */
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var onChangeHandler = scope.$eval(attrs.customOnChange);
            element.bind('change', onChangeHandler);
            element.on('$destroy', function () {
                element.unbind('change');
            });

        }
    };
});

GalleryApp.run(['$rootScope', function ($rootScope) {
    $rootScope.roomPictures = [];

}]);

var GalleryAppControllers = angular.module('GalleryAppControllers', ['ngRoute']);

GalleryAppControllers.controller('RoomsCtrl', function ($http, $rootScope, $scope, apiURL, $window) {
    $http.defaults.headers.common.Accept = "application/json";
    $http.defaults.headers.post = {'Content-Type': "application/json;charset=utf-8"};
    $http.defaults.headers.common.Authorization = "Bearer " + localStorage.getItem('token');

    // defaultny stav radio
    $scope.paintings = 4;

    // zobrazenie formularu na vytvorenie miestnosti
    $scope.showCreateForm = function () {
        $('#modal-room-title').text('Create new Room');
        $('#btn-update-room').hide();
        $('#btn-create-product').show();
        $('.paintingsCount').show();
        $scope.id_room_picture = '';
        $scope.name = "";
        $scope.description = "";
        $scope.images = [];
        for (var i = 0; i < 6; i++) {
            $scope.images.push({
                'index': i,
                'id': null,
                'height': '',
                'width': '',
                'name': '',
                'image': 'images/default.png',
                'pos': i
            });
        }
    };

    //zobrazenie miestnosti na editovanie
    $scope.showOne = function (id) {
        $http.get('/auth/room/' + id).success(function (data) {
            $scope.id_room_picture = id;
            $scope.name = data.name;
            $scope.description = data.description;
            $scope.paintings = data.num_picture;
            $scope.images = [];
            for (var i = 0; i < 6; i++) {
                $scope.images.push({
                    'index': i,
                    'id': null,
                    'name': '',
                    'width': '',
                    'height': '',
                    'image': "images/default.png",
                    'description': '',
                    'pos': i
                });
            }
            for (var i = 0; i < 6; i++) {
                if (data.pictures[i]) {
                    $scope.images[data.pictures[i].pos] = data.pictures[i];
                    $scope.images[data.pictures[i].pos].index = data.pictures[i].pos;
                    $scope.images[data.pictures[i].pos] = {
                        'index': data.pictures[i].pos,
                        'id': data.pictures[i].id_picture,
                        'name': data.pictures[i].name,
                        'width': Number(data.pictures[i].width),
                        'height': Number(data.pictures[i].height),
                        'image': data.pictures[i].image === 'no_picture' ? "images/default.png" : data.pictures[i].image,
                        'description': data.pictures[i].description,
                        'pos': data.pictures[i].pos
                    };
                }
            }

            $('.picture1').removeClass('p_count_6').addClass('p_count_4');
            if (data.num_picture == 6) {
                $('.picture1').removeClass('p_count_4').addClass('p_count_6');
            }
        }).error(errorHandler);
        $('#modal-room-title').text('Details/Edit Room');
        $('#modal-room-form').openModal();
        $('#btn-create-product').hide();
        $('#btn-update-room').show();
        $('.paintingsCount').hide();
    };

    // GET na vytiahnutie vsetkych miestnosti z DB
    $http.get(apiURL + '/all', {}).success(function (data) {
        $rootScope.roomPictures = data;
    }).error(errorHandler);

    $scope.validateForm = function (index) {
        var image = $scope.images[index];
        if ((image.width >= 20) && (image.width <= 700) && (image.height >= 20) && (image.height <= 700) && (image.name.length <= 20)
            && (image.image != "")) {
            $scope.images[index].valid = true;
            Materialize.toast('Painting added successfully.', 4000);
            angular.element('#pic' + (parseInt(index) + 1)).css('borderColor', '#81c784');
            $('#picture' + image.index + "_modal").closeModal();
        } else {
            angular.element('#button' + index).css('backgroundColor', '#ff8a80');
            angular.element('#pic' + (parseInt(index) + 1)).css('borderColor', '#ff8a80');
            $scope.images[index].valid = false;
            Materialize.toast('Error adding painting. You have to fill all the fields.', 2000);
        }
    };

    // POST vytvorenie novej miestnosti
    $scope.addRoom = function () {
        if ($scope.name) {
            $http.post(apiURL, {
                name: $scope.name,
                description: $scope.description,
                num_picture: Number($scope.paintings),
                pictures: $scope.images
            }).success(function (data) {
                $rootScope.roomPictures.push(data.room); // pridanie novej miestnosti do zoznamu
                // vycistenie inputov
                $scope.name = "";
                $scope.description = "";
                $scope.paintings = "4";
                for (var i = 0; i < 6; i++) {
                    angular.element('#pic' + (parseInt(i) + 1)).css('borderColor', 'grey');
                    $scope.images.push({
                        'index': i,
                        'id': null,
                        'height': '',
                        'width': '',
                        'name': '',
                        'image': '',
                        'pos': i
                    });
                }
                Materialize.toast('Room created', 5000);
                $('#modal-room-form').closeModal();
            }).error(errorHandler);
        } else {
            Materialize.toast("This form is not valid. Check if you have filled name of the room, or if your paintings are added correctly.", 5500);
        }
    };

    // POST update zvolenej miestnosti
    $scope.updateRoom = function () {
        if ($scope.name) {
            var o = {};
            o['pic1_name'] = $scope.name;
            $http.post(apiURL + '/update/' + $scope.id_room_picture, {
                name: $scope.name,
                description: $scope.description,
                num_picture: Number($scope.paintings),
                pictures: $scope.images
            }).success(function (data) {
                Materialize.toast('Room has been updated', 4000);
                for (var i = 0; i < 6; i++) {
                    angular.element('#pic' + (parseInt(i) + 1)).css('borderColor', 'grey');
                }
                $('#modal-room-form').closeModal();
            }).error(errorHandler);
        }
    };

    $scope.deleteRoom = function (room) {
        $http.delete(apiURL + '/delete/' + room.id_room_picture, {}).success(function (data) {
            var i = $rootScope.roomPictures.indexOf(room);
            if (i != -1) {
                $rootScope.roomPictures.splice(i, 1);
            }
            Materialize.toast('Room has been deleted successfully', 4000);
        }).error(errorHandler);
    };

    $scope.logout_user = function () {
        $http.post('/auth/logout/' + localStorage.getItem('user'), {}).success(function () {
            $window.location.href = 'auth.html';
            localStorage.setItem('token', null);
            localStorage.setItem('user', null);
        }).error(errorHandler);
    };

    $scope.uploadFile = function (event) {
        var index = event.target.getAttribute('data-index');
        var reader = new FileReader();
        reader.onload = function (e) {
            $scope.images[index].image = e.target.result;
        };
        reader.readAsDataURL(event.target.files[0]);
    };
});

var errorHandler = function (data, status, header, config, source) {
    if ((status === 401) && (source !== 'login')) {
        window.location.href = 'auth.html';
    }
    if ((status > 500) || !data || !data.message) {
        if (data && data.message) {
            alert("Failed to load application: " + status + ' ' + data.message);
        } else {
            alert("Failed to load application: " + status);
        }
        console.log(data);
        console.log(status);
        console.log(header);
        console.log(config);
    } else {
        Materialize.toast(data.message, 4000);
    }
};

GalleryAppControllers.controller('AuthCtrl', function ($http, $rootScope, $scope, apiURL, $window) {
    $http.defaults.headers.common.Accept = "application/json";
    $http.defaults.headers.post = {'Content-Type': "application/json;charset=utf-8"};

    // login uzivatela
    $scope.login_user = function () {
        $http.post('/login', {
            login: $scope.login,
            password: sha1($scope.password)
        }).success(function (data) {
            $window.location.href = 'admin.html';
            localStorage.setItem('token', data['token']);
            localStorage.setItem('user', $scope.login);
        }).error(function (data, status, header, config, source) {
            errorHandler(data, status, header, config, 'login');
            document.getElementById('login').className += ' invalid';
            document.getElementById('password').className += ' invalid';
            document.getElementById('login_info').className += ' user_info_displayed';
        })
    };
});


/* ---- jQuery ---- */

// Inicializacia Modal elementu od Materialized css
$(document).ready(function () {

    $('.modal-trigger').leanModal();
    $('select').material_select();

    // Obsluha radio-buttonov
    $('input:radio[name=group1]').change(function () {
        if (this.value == '6') {
            $('.picture1').removeClass('p_count_4').addClass('p_count_6');
        }
        if (this.value == '4') {
            $('.picture1').removeClass('p_count_6').addClass('p_count_4');
        }
    });
});
