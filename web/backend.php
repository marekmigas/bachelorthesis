<?php
/**
 * Created by PhpStorm.
 * User: marekmigas
 * Date: 28.06.16
 * Time: 11:12
 */

use Slim\Middleware\TokenAuthentication;
use Slim\Middleware\TokenAuthentication\UnauthorizedExceptionInterface;

require '../vendor/autoload.php';
require '../src/config.php';


class UnauthorizedException extends \Exception implements UnauthorizedExceptionInterface
{
}

$app = new Slim\App(['settings' => $config]);

$container = $app->getContainer();
$container['db'] = function ($c) {
    try {
        $db = $c['settings']['db'];
        $pdo = @new PDO(
            "mysql:host=" . $db['host'] . ";dbname=" . $db['dbname'],
            $db['user'],
            $db['pass']
        );
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        return $pdo;
    } catch (\PDOException $e) {
        header('Status: Internal Server Error', true, 500);
        echo json_encode([
            'message' => 'Database connection failed.'
        ]);
        exit;
    }
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c['settings']['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

$container['errorHandler'] = function ($c) {
    return function ($request, $response, $exception) use ($c) {
        /** @var \Monolog\Logger $logger */
        $logger = $c['logger'];
        $logger->error($exception->getMessage());
        $logger->error($exception->getTraceAsString());
        /** @var \Slim\Http\Response $response */
        $response = $c['response'];
        return $response->withStatus(500)
            ->withJson(['message' => 'Internal Server Error']);
    };
};

$authenticator = function ($request, TokenAuthentication $tokenAuth) use ($container) {
    /** @var \Monolog\Logger $logger */
    $logger = $container['logger'];
    /** @var \PDO $db */
    $db = $container['db'];
    # Search for token on header, parameter, cookie or attribute
    $token = $tokenAuth->findToken($request);
    $stmt = $db->prepare(
        "SELECT TIMESTAMPDIFF(SECOND, NOW(), date_expired) AS remaining FROM admin WHERE token = :token"
    );
    $stmt->bindValue(':token', $token);
    $stmt->execute();
    $data = $stmt->fetch();
    if (!$data) {
        throw new UnauthorizedException("Token invalid.");
    }
    $logger->info(var_export($data, true));
    $remaining = $data['remaining'];
    $logger->info("Token remaining time " . $remaining);
    if ($remaining <= 0) {
        throw new UnauthorizedException("Token expired.");
    }
};

$app->add(new TokenAuthentication([
    'path' => '/auth/',
    'secure' => true,
    'relaxed' => ['localhost', 'km-gallery.com'],
    'authenticator' => $authenticator
]));

require '../src/routes.php';
$app->run();
