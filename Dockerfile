FROM php:5.6-apache
ENV DEBIAN_FRONTEND noninteractive
ENV COMPOSER_ALLOW_SUPERUSER 1

# install git
RUN apt update \
	&& apt-get install -y \
		git \
        unzip \
        zip \
	--no-install-recommends \
	&& rm -rf /var/lib/apt/lists/*

# install PHP extensions
RUN docker-php-ext-install pdo \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mysql \
    && a2enmod rewrite \
    && pecl config-set php_ini /usr/local/etc/php.ini \
    && yes | pecl install xdebug \
    && echo "zend_extension=$(find /usr/local/lib/php/extensions/ -name xdebug.so)" > /usr/local/etc/php/conf.d/xdebug.ini    

# install composer
RUN cd \
  && curl -sS https://getcomposer.org/installer | php \
  && ln -s /root/composer.phar /usr/local/bin/composer

ENV APACHE_LOCK_DIR=/var/apache/lock/

COPY . /code/
RUN cp /code/docker/php.ini /usr/local/etc/php/php.ini \
    && cp /code/docker/000-default.conf /etc/apache2/sites-available/ \
    && mkdir -p /var/apache/lock \
    && chmod a+rw /var/apache/lock \
    && chmod a+rw /code/logs

WORKDIR /code/
RUN composer install
